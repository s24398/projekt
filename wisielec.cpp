#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
//zmienne

int c_max = 0;
int r_max  = 0;


char const *slownik[5] = {"flaszka","imposybilizm","orangutan", "trzmiel","konnica"};



void wisielec1(int r,int c)
{
    move(r,c);
    printw("|");
    move(r+1,c);
    printw("|");
    move(r+2,c);
    printw("|");
    move(r+3,c);
    printw("|");
    move(r+4,c);
    printw("|");
}
void wisielec2(int r,int c)
{
    move(r-1,c+1);
    printw("_");
    move(r-1,c+2);
    printw("_");
    move(r-1,c+3);
    printw("_");

}
void wisielec3(int r,int c)
{
    move(r,c+3);
    printw("|");
}
void wisielec4(int r,int c)
{
    move(r+1,c+3);
    printw("O");
}
void wisielec5(int r, int c)
{
    move(r+2,c+3);
    printw("|");
}
void wisielec6(int r, int c)
{
    move(r+2,c+2);
    printw("/");
}
void wisielec7(int r, int c)
{
    move(r+2,c+4);
    printw("\\");
}
void wisielec8(int r, int c)
{
    move(r+3,c+4);
    printw("\\");
}
void wisielec9(int r, int c)
{
    move(r+3,c+2);
    printw("/");
}
int show_word(char ch, char const* word, int count, char* matched_letters)
{
    int matched_letter_count = -1;

    move(10,0);

    for(int i=0; i < count; i++)
    {
      if (ch == word[i])
      {
        if (matched_letters[i] == 1)
        {
            matched_letter_count = 0;
        }
        else
        {
           if(matched_letter_count == -1) matched_letter_count=0;
           matched_letters[i] = 1;
           matched_letter_count++;
        }
      }
    }

    for(int i=0; i < count; i++)
    {
        if (matched_letters[i] == 1)
        {
           printw("%c ", word[i]);
        }
        else
        {
            printw("_ ");
        }
    }

    return matched_letter_count;
}

int main()
{

    int matched_letter_count = 0;
    int total_matched_letter_count = 0;
    int total_unmatched_letter_count = 0;
    char matched_letters[20] = {0};
    int slownik_size = sizeof(slownik)/sizeof(char*);
    unsigned int nr=0;
    //start
    initscr();
    //Pobieranie wartości okna do zmiennych
    getmaxyx( stdscr, r_max, c_max );
    do
    {
      move(0,0);
      printw("podaj numer slowa od 0 do %d ", slownik_size-1);
      char nr_char = getch();
      if (nr_char>47 && nr_char <58)
      {
        nr = atoi(&nr_char);
      }
      else
      {
        nr = slownik_size;
      }

    }while(nr >= slownik_size);
    move(1,0);
    //printw("podany nr to %d",nr);
    int letter_count= strlen(slownik[nr]);
    //move(2,0);
    //printw("liczba liter slowa %s to %d",slownik[nr], letter_count);
    show_word(0, slownik[nr], letter_count, matched_letters);
    do
    {
        move(3,0);
        printw("podaj litere ");
        char letter = getch();

        matched_letter_count = show_word(letter, slownik[nr], letter_count, matched_letters);

        if (matched_letter_count == -1)
        {
            total_unmatched_letter_count++;
            switch (total_unmatched_letter_count)
            {
                case 1:
                    wisielec1(r_max/2,c_max/2);
                    break;
                case 2:
                    wisielec2(r_max/2,c_max/2);
                    break;
                case 3:
                    wisielec3(r_max/2,c_max/2);
                    break;
                case 4:
                    wisielec4(r_max/2,c_max/2);
                    break;
                case 5:
                    wisielec5(r_max/2,c_max/2);
                    break;
                case 6:
                    wisielec6(r_max/2,c_max/2);
                    break;
                case 7:
                    wisielec7(r_max/2,c_max/2);
                    break;
                case 8:
                    wisielec8(r_max/2,c_max/2);
                    break;
                case 9:
                    wisielec9(r_max/2,c_max/2);
                    break;
                default:
                    ;
            }
        }
        else
        {
           total_matched_letter_count += matched_letter_count;
        }


    }while(total_matched_letter_count < letter_count && total_unmatched_letter_count < 9 );

    move(13,0);
    if(total_matched_letter_count == letter_count)
    {
       printw("Gratulacje  !");
    }
    else
    {
        printw("Ooooops, probuj raz jeszcze !");
    }
    move(14,0);
    printw("Nacisnij dowolny klawisz...");
    getch();
    endwin();
}
